//![SECTION] DEPENDENCIES
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const User = require("./models/User");
const secret = "MuraStoreAPI";



module.exports.createAccessToken = (user) => {
    console.log(user);

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
    };
    console.log(data);
    return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {

    let token = req.headers.authorization;

    if (typeof token === "undefined") {


        return res.send({ auth: "Failed. No Token" });
    } else {
        console.log(token);

        token = token.slice(7, token.length);
        console.log(token);
       
        jwt.verify(token, secret, (err, decodedToken) => {
            if (err) {
                return res.send({
                    auth: "Failed",
                    message: err.message,
                });
            } else {
                console.log(decodedToken);

                req.user = decodedToken;

                next();
            }
        });
    }
};



module.exports.verifyAdmin = (req, res, next) => {
    if (req.user.isAdmin) {
        next();
    } else {
        return res.send({
            auth: "Failed",
            message: "Action Forbidden",
        });
    }
};
