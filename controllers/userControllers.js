const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcryptjs');
const auth = require('../auth')


// REGISTER USER

module.exports.registerUser = (req, res) => {

	console.log(req.body);

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	});
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
};

module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

module.exports.loginUser = (req, res) => {

	console.log(req.body);

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send("No user found in the database");

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			console.log(isPasswordCorrect);
			 
			if(isPasswordCorrect){

				return res.send({accessToken: auth.createAccessToken(foundUser)})

			} else {

				return res.send("Incorrect password, please try again")
			}
		}
	})
	.catch(err => res.send(err));
};

module.exports.updateUserDetails = (req, res) => {
	
	console.log(req.body); //input for new values
	console.log(req.user.id); // check the logged in user's id

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo
	};

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));	
};

module.exports.updateAdmin = (req, res) => {

	console.log(req.user.id); // id of the logged in user

	console.log(req.params.id); // id of the user we want to update

	let updates = {

		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

//! [SECTION] Order Products here
module.exports.order = async(req, res) => {
    console.log(req.user.id);
    console.log(req.body.productId);
    console.log(req);

    if (req.user.isAdmin) {
        return res.send("Action Forbidden");
    }

    let isUserUpdated = await User.findById(req.user.id).then((user) => {
        let newPurchase = {
            productId: req.body.productId,
            productPrice: req.body.productPrice,
        };
        user.productOrders.push(newPurchase);
        user.totalAmount.push(newPurchase.productPrice);

        console.log(user.productOrders);
        let sum = user.totalAmount.reduce((prev, current) => prev + current);

        User.findByIdAndUpdate(req.user.id, { sumOrders: sum }, { new: true })
            .then((user) => user)
            .catch((err) => err.message);

			return user
            .save()
            .then((user) => true)
            .catch((err) => err.message);
    });

    if (isUserUpdated !== true) {
        return res.send({ message: isUserUpdated });
    }

    let isProductUpdated = await Product.findById(req.body.productId).then(
        (product) => {
            let customer = {
                userId: req.user.id,
            };

            product.customers.push(customer);

            return product
                .save()
                .then((product) => true)
                .catch((err) => err.messaqge);
        }
    );

    if (isProductUpdated !== true) {
        //false
        return res.send({ message: isProductUpdated });
    }

    if (isUserUpdated && isProductUpdated) {
        return res.send({ message: "User ordered successfully!" });
    }
};

module.exports.getUserOrder = (req, res) => {

    User.findById(req.user.id)

    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};


module.exports.viewAllOrdersAdmin = (req, res) => {
    let arr = [];
    Product.find({})
        .then((result) => {
            result.forEach((item) => {
                console.log(item);
                item.customers.length !== 0 &&
                    arr.push({
                        name: item.name,
                        description: item.description,
                        price: item.price,
                        customers: item.customers,
                    });
            });

            res.send({ arr });
        })
        .catch((err) => res.send(err));
};