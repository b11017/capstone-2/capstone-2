const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},

	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]

	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	activateStatus: {
		type: Boolean,
		default: true
	},
	productOrders: [{
	        productId: {
	            type: String,
	            required: [true, "Product Id is required."],
	        },

	        productPrice: {
	            type: Number,
	            required: [true, "Price is required."],
	        },

	        status: {
	            type: String,
	            default: "Ordered",
	        },

	        dateOrdered: {
	            type: Date,
	            default: new Date(),
	        },
	    }, ],

	    totalAmount: [{
	        type: Number,
	        default: 0,
	    }, ],
	    sumOrders: Number,
	});

module.exports = mongoose.model("User", userSchema);