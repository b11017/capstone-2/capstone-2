//! [SECTION] dependencies
const express = require("express");
//http method
const router = express.Router();

const auth = require("../auth");

//* for verification Users/Admin
const { verify } = auth;
const { verifyAdmin } = auth;

//![SECTION] Imported Modules
const userControllers = require("../controllers/userControllers");

//* Check if registered users
router.post('/', userControllers.registerUser);

//*get all users
router.get("/", userControllers.getAllUsers);

//*login user
router.post("/login", userControllers.loginUser);


router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

//*Update user Admin
router.put(
    "/updateUserAdmin/:id",
    verify,
    verifyAdmin,
    userControllers.updateAdmin
);

//* order products
router.post("/order", verify, userControllers.order);

//* view all orders per user
router.get("/getUserOrder", verify, userControllers.getUserOrder);

router.get(
    "/viewAllOrdersAdmin",
    verify,
    verifyAdmin,
    userControllers.viewAllOrdersAdmin
);


module.exports = router;